/*  THIS CODE IS AUTOGENERATED  
*  IF YOU ****REAAALLLY**** NEED TO ADD CHANGES TRY USING ANOTHER PARTIAL OR SOMETHING. 
*  YOU WILL LOSE YOUR CHANGES ON THIS FILE WHEN REIMPORTING SFX!!! 
*/
namespace Trovador 
{
    public partial class SoundPlayer 
    { 
        
        public static void Play_Body_Impact() 
        {
            var source = GetAudioSource();
            source.clip = loadedClips["Body_Impact"];
            source.Play();
        }
 
    
        public static void Play_Bubble_Pop() 
        {
            var source = GetAudioSource();
            source.clip = loadedClips["Bubble_Pop"];
            source.Play();
        }
 
    
        public static void Play_CartoonBlink() 
        {
            var source = GetAudioSource();
            source.clip = loadedClips["CartoonBlink"];
            source.Play();
        }
 
    
        public static void Play_Door_Opening() 
        {
            var source = GetAudioSource();
            source.clip = loadedClips["Door_Opening"];
            source.Play();
        }
 
    
        public static void Play_Meow() 
        {
            var source = GetAudioSource();
            source.clip = loadedClips["Meow"];
            source.Play();
        }
 
    
        public static void Play_Sizzle() 
        {
            var source = GetAudioSource();
            source.clip = loadedClips["Sizzle"];
            source.Play();
        }
 
    
        public static void Play_UI_Click_Strong() 
        {
            var source = GetAudioSource();
            source.clip = loadedClips["UI_Click_Strong"];
            source.Play();
        }
 
 
    } 
}