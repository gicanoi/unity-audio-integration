using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Trovador
{
    public class SoundPlayerGenerator : MonoBehaviour
    {
        [MenuItem("Trovador/Import SFX")]
        static void CreatePlayerClassFromImportedSfx()
        {
            File.WriteAllText(SoundPlayerGeneratorConfiguration.PLAYER_PARTIAL_CLASS_PATH,
                GeneratePlayerPartialClass(GeneratePlayMethods()));
            
            Debug.Log("SFX Files imported! Go to your IDE so Unity will become aware of the changes");
        }

        static string GeneratePlayerPartialClass(string playMethods) =>
            SoundPlayerGeneratorHelper.CommentTemplate +
            SoundPlayerGeneratorHelper.SoundPlayerPartialTemplate.Replace(SoundPlayerGeneratorHelper.PLAY_METHODS, playMethods);

        static string GeneratePlayMethods()
        {
            var clips = Resources.LoadAll<AudioClip>(SoundPlayerGeneratorConfiguration.SFX__RESOURCES_PATH);
            var playMethods = new StringBuilder();
            Debug.Log($@"Importing {clips.Length} audio clips");
            foreach (var clip in clips)
                playMethods.AppendLine(CreatePlayMethod(clip));
            return playMethods.ToString();
        }

        static string CreatePlayMethod(AudioClip clip) => 
            SoundPlayerGeneratorHelper.PlaySoundMethodTemplate.Replace(SoundPlayerGeneratorHelper.CLIP_NAME, SoundPlayerGeneratorHelper.SanitizeFileName(clip));
    }
}