using UnityEngine;

namespace Trovador
{
    public class AudioSourcePool : MonoBehaviour
    {
        [SerializeField] int maxSourcesInPool = 8;
        
        AudioSource[] sources;
        int current;

        void Awake()
        {
            sources = new AudioSource[maxSourcesInPool];
            InstantiateAudioSources();
        }

        public AudioSource GetAudioSource()
        {
            FindNextAvailable();
            return sources[current];
        }

        void InstantiateAudioSources()
        {
            for (var i = 0; i < maxSourcesInPool; i++)
                sources[i] = gameObject.AddComponent<AudioSource>();
        }

        void FindNextAvailable()
        {
            while (sources[current].isPlaying)
            {
                current++;
                if (current >= maxSourcesInPool)
                    current = 0;
            }
        }
    }
}