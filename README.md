# Trovador SFX Importer

<details><summary>ENGLISH</summary>

Have you ever neglected SFX integration during a game jam? 

You hate when sound designers send a ton of files and now you have to add them to your project?

This simple package will do the work for you!*

# How to?

## 1 - Download and import the package

[Download the package](https://gitlab.com/gicanoi/unity-audio-integration/-/raw/main/Download/TrovadorSfxImport.unitypackage?inline=false)

Double click the package file to add it to your Unity project.
![](ReadmeFiles/1%20-%20ImportPackage.png)

## 2 - Import ALL THE FILES!!!!oneeleven11!!!

Select all files

![](ReadmeFiles/2%20-%20SelectAll.png)

## 3 - Add TrovadorSfxImport to project hierarchy

Just do it!

![](ReadmeFiles/3%20-%20DragToHierarchy.png)

## 4 - Add some audio clips 

Drop your audio clips under `Assets/Resources/SFX`. Create this directory if it't not already in your project.
That's the default directory, you can change the configuration in `SoundPlayerGeneratorConfiguration.cs` but it shouldn't be neccesary. 

It should look like this:

![](ReadmeFiles/4%20-%20AddSomeSfxFiles.png)

## 5 - Import the SFX files

You should have a Trovador menu with an ImportSFX item. Click it.

![](ReadmeFiles/5%20-%20TrovadorImport.png)

## 6 - Your SFX should be imported now

You should see this message in the console.

![](ReadmeFiles/6%20-%20ConsoleMessage.png)

## 7 - Wait, what? I did not write this code! 

A new cs file is available now, it has code to trigger the imported audio clips.

![](ReadmeFiles/7%20-%20TrovadorSfxPlayerPartial.png)

## 8 - It's time to test this

Create an empty game object and add a script to it.

![](ReadmeFiles/8%20-%20CreateEmpty.png)

![](ReadmeFiles/9%20-%20AddAScript.png)

## 9 - Let's add a line 

As you can see there are methods now to trigger every one of the SFX you imported. The method names depend on your audio clips filenames, so try to keep it tidy!

![](ReadmeFiles/10%20-%20Autocomplete.png)

## 10 - Choose one of the SFX and run the project

If everything went fine you should hear the SFX when your game starts. 

![](ReadmeFiles/11%20-%20ChooseOne.png)

## 11 - Have fun!


















*Sorry, the marketing team requested this.

</details>



<details><summary>ESPAÑOL</summary>

Alguna vez dejaste para último momento la integración de efectos de sonido en una game jam?

Detestas cuando te mandan una tonelada de archivos de sonido para integrar?

Este paquete hace todo el trabajo por vos!*

# Cómo?

## 1 - Bajar e importar el paquete

[Bajar el paquete](https://gitlab.com/gicanoi/unity-audio-integration/-/raw/main/Download/TrovadorSfxImport.unitypackage?inline=false)

Hace doble clic en el paquete para importarlo en tu proyecto de Unity.
![](ReadmeFiles/1%20-%20ImportPackage.png)

## 2 - Importar TODOS LOS ARCHIVOOOS!!!!unoonce11!!!

Seleccionar todos los archivos.

![](ReadmeFiles/2%20-%20SelectAll.png)

## 3 - Agregar TrovadorSfxImport a la jerarquía

Hacelo!

![](ReadmeFiles/3%20-%20DragToHierarchy.png)

## 4 - Agrega algunos archivos de sonido

Arrastra tus archivos de sonido a `Assets/Resources/SFX`. Si el directorio no existe crealo.
Esa es la ruta por defecto, se puede cambiar la configuración en `SoundPlayerGeneratorConfiguration.cs` pero no debería ser necesario.

Debería quedar así:

![](ReadmeFiles/4%20-%20AddSomeSfxFiles.png)

## 5 - Importa los efectos de sonido

Debería aparecer un menu "Trovador" con el item "Import SFX". Hace clic ahí.

![](ReadmeFiles/5%20-%20TrovadorImport.png)

## 6 - Los efectos de sonido deberían estar importados

La consola debería mostrar un mensaje como este

![](ReadmeFiles/6%20-%20ConsoleMessage.png)

## 7 - Para un toque, yo no escribí este código!

Ahora hay un nuevo archivo de C#, tiene el código necesario para disparar los clips importados.

![](ReadmeFiles/7%20-%20TrovadorSfxPlayerPartial.png)

## 8 - Hora de probarlo

Crea un _game object_ vacío y agregale un _script_. 

![](ReadmeFiles/8%20-%20CreateEmpty.png)

![](ReadmeFiles/9%20-%20AddAScript.png)

## 9 - Tirate una línea

Como podes ver ahora hay métodos para disparar cada uno de los sonidos importados. Los nombres de los métodos dependen de los nombres de archivo, trata de mantener la prolijidad!

![](ReadmeFiles/10%20-%20Autocomplete.png)

## 10 - Elegi uno de los sonidos y corre el proyecto

Si todo salió bien deberías escuchar el sonido cuando el juego arranque.


![](ReadmeFiles/11%20-%20ChooseOne.png)

## 11 - Divertite!























*Perdón, los de marketing nos pidieron que dijéramos eso.
</details>



























